{
  description = "zremap nix flake";

  outputs = {
    self,
    nixpkgs,
  }: {
    defaultPackage.x86_64-linux = with import nixpkgs {system = "x86_64-linux";};
      stdenv.mkDerivation {
        pname = "zremap";
        version = "0.1";
        src = self;
        nativeBuildInputs = [zig.hook];
        buildInputs = [libevdev pkg-config];
        buildPhase = ''
          NIX_CFLAGS_COMPILE="-isystem $(pkg-config --variable=includedir libevdev)/libevdev-1.0 $NIX_CFLAGS_COMPILE"
        '';
      };

    devShells.x86_64-linux.default = with import nixpkgs {system = "x86_64-linux";};
      mkShell {
        nativeBuildInputs = [zig lldb];
        buildInputs = [
          libevdev
          pkg-config
        ];

        shellHook = ''
          NIX_CFLAGS_COMPILE="-isystem $(pkg-config --variable=includedir libevdev)/libevdev-1.0 $NIX_CFLAGS_COMPILE"
                 echo "happy hacking!"
        '';
      };
  };
}
